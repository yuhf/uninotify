package com.iotechn.uninotify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UninotifyApplication {

	public static void main(String[] args) {
		SpringApplication.run(UninotifyApplication.class, args);
	}

}

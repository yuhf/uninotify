package com.iotechn.uninotify.exception;

/**
 * Created by rize on 2019/6/30.
 */
public class NotifyExceptionDefinition {

    public static final ServiceExceptionDefinition NOTIFY_API_REGISTER_FAILED =
            new ServiceExceptionDefinition(9999, "api注册失败");

    public static final ServiceExceptionDefinition NOTIFY_UNKNOWN_EXCEPTION =
            new ServiceExceptionDefinition(10000, "系统未知异常");

    public static final ServiceExceptionDefinition NOTIFY_REQUEST_EXPIRED =
            new ServiceExceptionDefinition(10001, "请求已经过期");

    public static final ServiceExceptionDefinition NOTIFY_PARAM_CHECK_FAILED =
            new ServiceExceptionDefinition(10002, "参数校验失败");

    public static final ServiceExceptionDefinition NOTIFY_API_NOT_EXISTS =
            new ServiceExceptionDefinition(10003, "API不存在");

    public static final ServiceExceptionDefinition NOTIFY_SIGN_CANNOT_BE_NULL =
            new ServiceExceptionDefinition(10004, "签名不能为空");

    public static final ServiceExceptionDefinition NOTIFY_SIGN_NOT_INCORRECT =
            new ServiceExceptionDefinition(10005, "请求签名不正确");

    public static final ServiceExceptionDefinition NOTIFY_APP_ID_CANNOT_BE_NULL =
            new ServiceExceptionDefinition(10007, "APPID不能为空");

    public static final ServiceExceptionDefinition NOTIFY_ADMIN_PERMISSION_DENY =
            new ServiceExceptionDefinition(10008, "管理员权限不足");

    public static final ServiceExceptionDefinition NOTIFY_IO_EXCEPTION =
            new ServiceExceptionDefinition(10009, "网络异常");

    public static final ServiceExceptionDefinition NOTIFY_THIRD_PART_EXCEPTION =
            new ServiceExceptionDefinition(10010, "第三方服务异常");


    public static final ServiceExceptionDefinition NOTIFY_APP_ID_NOT_EXIST =
            new ServiceExceptionDefinition(11001, "AppId尚未注册");

    public static final ServiceExceptionDefinition NOTIFY_USER_HAS_BEEN_REGISTERED =
            new ServiceExceptionDefinition(11002, "该用户已经被注册了");

    public static final ServiceExceptionDefinition NOTIFY_WX_RESPONSE_FAILED =
            new ServiceExceptionDefinition(11003, "微信公众平台回复失败");

    public static final ServiceExceptionDefinition NOTIFY_USER_NOT_EXIST =
            new ServiceExceptionDefinition(11004, "该用户尚未注册");


}

package com.iotechn.uninotify.exception;

/**
 * Created by rize on 2019/6/30.
 */
public class NotifyServiceException extends ServiceException {

    public NotifyServiceException(ServiceExceptionDefinition exceptionDefinition) {
        super(exceptionDefinition);
    }

    public NotifyServiceException(String msg, int code) {
        super(msg, code);
    }

}
